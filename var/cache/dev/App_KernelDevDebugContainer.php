<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerAOQO0Hi\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerAOQO0Hi/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerAOQO0Hi.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerAOQO0Hi\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerAOQO0Hi\App_KernelDevDebugContainer([
    'container.build_hash' => 'AOQO0Hi',
    'container.build_id' => 'eae9a8e4',
    'container.build_time' => 1590057382,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerAOQO0Hi');
